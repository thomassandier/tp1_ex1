# tp1_ex1
Ce code est un programme Python qui définit plusieurs fonctions pour effectuer des opérations sur des nombres complexes. Les opérations implémentées sont l'addition, la soustraction, la multiplication et la division de nombres complexes.

La fonction complex_sum calcule la somme de deux nombres complexes en additionnant leurs parties réelles et imaginaires. Elle renvoie le résultat sous forme d'une liste.

La fonction complex_subtract calcule la différence de deux nombres complexes en soustrayant leurs parties réelles et imaginaires. Elle renvoie le résultat sous forme d'une liste.

La fonction complex_multiply calcule le produit de deux nombres complexes en utilisant la formule appropriée pour les parties réelles et imaginaires. Elle renvoie le résultat sous forme d'une liste.

La fonction complex_divide calcule la division de deux nombres complexes en utilisant la formule appropriée pour les parties réelles et imaginaires. Elle renvoie le résultat sous forme d'une liste.

Le code teste ensuite ces fonctions en utilisant deux nombres complexes prédéfinis. Les résultats des opérations sont affichés à l'aide de la fonction print.
