#auteur : Thomas Sandier

# Définition de la fonction complex_sum pour calculer la somme de deux nombres complexes
def complex_sum(complex1, complex2):
    real_part = complex1[0] + complex2[0]  # Calcul de la partie réelle
    imaginary_part = complex1[1] + complex2[1]  # Calcul de la partie imaginaire
    return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

# Définition de la fonction complex_subtract pour calculer la différence de deux nombres complexes
def complex_subtract(complex1, complex2):
    real_part = complex1[0] - complex2[0]  # Calcul de la partie réelle
    imaginary_part = complex1[1] - complex2[1]  # Calcul de la partie imaginaire
    return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

# Définition de la fonction complex_multiply pour calculer le produit de deux nombres complexes
def complex_multiply(complex1, complex2):
    real_part = complex1[0] * complex2[0] - complex1[1] * complex2[1]  # Calcul de la partie réelle
    imaginary_part = complex1[0] * complex2[1] + complex1[1] * complex2[0]  # Calcul de la partie imaginaire
    return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

# Définition de la fonction complex_divide pour calculer la division de deux nombres complexes
def complex_divide(complex1, complex2):
    denominator = complex2[0] ** 2 + complex2[1] ** 2  # Calcul du dénominateur
    real_part = (complex1[0] * complex2[0] + complex1[1] * complex2[1]) / denominator  # Calcul de la partie réelle
    imaginary_part = (complex1[1] * complex2[0] - complex1[0] * complex2[1]) / denominator  # Calcul de la partie imaginaire
    return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

# Tester les fonctions
complex1 = [4.67, 5.89]  # Premier nombre complexe
complex2 = [2.34, 3.45]  # Deuxième nombre complexe

print("Somme :", complex_sum(complex1, complex2))  # Affiche la somme des deux nombres complexes
print("Différence :", complex_subtract(complex1, complex2))  # Affiche la différence des deux nombres complexes
print("Produit :", complex_multiply(complex1, complex2))  # Affiche le produit des deux nombres complexes
print("Division :", complex_divide(complex1, complex2))  # Affiche la division des deux nombres complexes
